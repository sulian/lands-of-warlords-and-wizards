-- WaW

io.stdout:setvbuf('no')

-- love.graphics.setColor(1, 0, 0, 1) Red

-- Required for pixel art
love.graphics.setDefaultFilter("nearest")

-- ZeroBraneStudio
if arg[#arg] == "-debug" then require("mobdebug").start() end

if pcall(require, "lldebugger") then
    require("lldebugger").start()
end

DEBUG = false

local MOUSE = require("src/global/mouse")
local terrains = require("src/maps/terrains")
local units = require("src/units/units")

Tilemap = {
    { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2 },
    { 1, 2, 2, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2 },
    { 1, 2, 3, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2 },
    { 1, 3, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2 },
    { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2 },
    { 1, 1, 3, 3, 3, 3, 1, 1, 1, 1, 1, 1, 1, 2 },
    { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2 },
    { 1, 1, 1, 1, 2, 2, 2, 1, 1, 1, 1, 1, 1, 2 },
    { 1, 1, 1, 1, 1, 1, 1, 1, 1, 4, 4, 4, 4, 4 },
    { 1, 1, 1, 1, 1, 1, 1, 4, 4, 4, 4, 4, 4, 4 },
    { 1, 1, 1, 1, 1, 1, 1, 4, 4, 4, 4, 4, 4, 4 },
    { 2, 2, 2, 2, 2, 2, 2, 4, 4, 4, 4, 4, 4, 4 }
}

SizeMap = #Tilemap
myKey = nil

TILE = 64

Select = {}
Select.c = 1
Select.l = 1

listUnit = {}

function createUnit (pType, pLine, pColumn)
    local Unit = {}
    Unit.Type = pType
    Unit.HP = dbUnits[pType].HP
    Unit.Line = pLine
    Unit.Column = pColumn

    table.insert(listUnit, Unit)
end

function startGame()
    createUnit("TANK", 1 , 1)
    createUnit("INFANTRY", 5 , 5)
end

function love.load()
    startGame()

    MOUSE:load()
end

function love.update(dt)
    mouse_x, mouse_y = love.mouse.getPosition()

    MOUSE:update()
end

function love.draw()
    local x, y = 10, 100
    local sx, sy = 0, 0

    -- for l = 1, #Tilemap do
    for l = 1, 12 do
        x = 10

        for c = 1, 14 do
            local typeT = Tilemap[l][c]

            if Tilemap[l][c] == 4 then
                love.graphics.setColor(0, .41, 0.58, 1)
                love.graphics.rectangle("fill", x, y, TILE, TILE)
            else
                love.graphics.setColor(1, 1, 1, 1)
                love.graphics.draw(dbTerrains[typeT].Img, x, y, 0, 8, 8)
            end

            if c == Select.c and l == Select.l then
                sx = x
                sy = y
            end
            x = x + TILE + 1
        end
        y = y + TILE + 1
    end

    local xStat = TILE * 14 + 10 + 20
    local yStat = 100 + 5

    love.graphics.setColor(1, 1, 1, 1)
    for u = 1, #listUnit do
        local unit = listUnit[u]
        local x = (unit.Line - 1) * TILE + 10
        local y = (unit.Column - 1) * TILE + 100

        -- Take image directly in the db, image is not loaded in "Unit"
        love.graphics.draw(dbUnits[unit.Type].Img, x, y, 0, 4, 4)

        if Select.c == unit.Column and Select.l == unit.Line then
            love.graphics.print(unit.Type, xStat, yStat)
            love.graphics.print("HP: " .. unit.HP, xStat, yStat + 16)
            love.graphics.print("Gas: " .. dbUnits[unit.Type].Gas, xStat, yStat + 16*2)
            love.graphics.print("Range: " .. dbUnits[unit.Type].Range, xStat, yStat + 16*3)
        end
    end

    local terrainType = Tilemap[Select.l][Select.c]
    love.graphics.print("Terrain: " .. dbTerrains[terrainType].Name, xStat + 100, yStat)

    love.graphics.setColor(1, 1, 1, 1)
    love.graphics.rectangle("line", sx, sy, TILE, TILE)

    -- Debug information
    if (DEBUG) then
        love.graphics.print("DEBUG: " .. love.timer.getFPS(), 10, 1)
        love.graphics.print("Key " .. myKey .. " pressed", 10, 16)
        love.graphics.print("mouse X: " .. mouse_x .. ", Y: " .. mouse_y, 10, 16*2)
        love.graphics.print("item in Tilemap: " .. #Tilemap, 10, 16*3)
    end

    MOUSE:draw()
end

function love.keypressed(key)
    myKey = key
    if key == "right" and Select.c < 14 then
        Select.c = Select.c + 1
    end
    if key == "left" and Select.c > 1 then
        Select.c = Select.c - 1
    end
    if key == "up" and Select.l > 1 then
        Select.l = Select.l - 1
    end
    if key == "down" and Select.l < 12 then
        Select.l = Select.l + 1
    end
    if key == "d" then
        DEBUG = not DEBUG
    end
    if key == "escape" then
        love.event.quit()
    end

    MOUSE:keypressed(key)    
end

function love.mousepressed(x, y, button, istouch, presses)
    MOUSE:mousepressed(x, y, button, istouch, presses)
end

function love.wheelmoved(x, y)
    MOUSE:wheelmoved(x, y)    
end

function love.mousemoved(x, y, dx, dy)
    MOUSE:mousemoved(x, y, dx, dy)
end