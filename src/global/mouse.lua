local MOUSE = {}

local x, y = 0, 0

function MOUSE:load()
    MOUSE = love.graphics.newImage("img/8_white.png")
    love.mouse.setVisible(false)
end

function MOUSE:update()
    x, y = love.mouse.getPosition()
end

function MOUSE:draw()
    if love.mouse.isVisible() == false then
        love.graphics.draw(MOUSE, x, y)
    end
end

function MOUSE:keypressed(key)
    if key == "m" then
        love.mouse.setVisible(not (love.mouse.isVisible()))
    end
end

function MOUSE:mousepressed(x, y, button, istouch, presses )
    if (DEBUG) then
        print ("x: " .. tostring(x) .. " / y: " .. tostring(y) .. " / button: " .. tostring(button) .. " / istouch: " .. tostring(istouch) .. " / presses: " .. tostring(presses))
    end
end

function MOUSE:wheelmoved(x, y)
    if y > 0 then
        if (DEBUG) then
            print ("Mouse wheel moved up")
        end
    elseif y < 0 then
        if (DEBUG) then
            print ("Mouse wheel moved down")
        end
    end
end

function MOUSE:mousemoved(x, y, dx, dy)
    if (DEBUG) then
        if love.mouse.isDown(1) then
            print ("Button 1 pressed while moved")
        end
    end
end

return MOUSE